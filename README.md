# Bootstrap Quick Tabs

This module provides a Bootstrap tabs "renderer" for to the Quick Tabs module that
enables site-builders to add Bootstrap-style tabs or pills to a page. It provides
a Bootstrap accordion "renderer" to render content as collapsible panels. This
module should work with the Bootstrap theme. No other Bootstrap themes were tested
with this module.

## Requirements

This module requires the following module:

[Quick Tabs](https://www.drupal.org/project/quicktabs)

## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration

1. Go to Administration » Structure » Quick Tabs
2. Select "bootstrap tabs" or "bootstrap accordion" as renderer.

## Options

See [Bootstrap Quick Tabs documentation](https://www.drupal.org/docs/contributed-modules/bootstrap-quick-tabs)

More information on Bootstrap Tabs can be found on the Bootstrap documentation site:
https://getbootstrap.com/docs/3.3/components/#nav
https://getbootstrap.com/docs/3.3/javascript/#collapse

Tabs Styles:
Tabs (default)
Pills

Position of tabs/pills:
Tabs/pills on the top (default)
Tabs/pills on the left
Tabs/pills on the right
Tabs/pills on the bottom
Tabs/pills justified on the top
Tabs stacked

Effect of tabs:
Fade (default off)

## Maintainers

- Shelane French - [shelane](https://www.drupal.org/u/shelane)
- Justin Shinn - [justinshinn](https://www.drupal.org/u/justinshinn)
- Jim Gauthier - [usurper](https://www.drupal.org/u/usurper)
